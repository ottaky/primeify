// top banner
document.querySelector('#aiv-cl-main-middle > div > div > div._1gQKv6.u-collection.tst-collection._3WQsXf > div > div').remove();
// prime flashes
document.querySelectorAll('li > div > div > div > a > div:nth-child(3)').forEach(div => div.remove());

let myStyle = document.createElement('style')

myStyle.innerHTML = `
@media(max-width: 1800px) and (min-width: 1500px) {
  .min-width-storefront .${document.getElementsByClassName('tst-card-wrapper')[0].classList[0]} {
    width: calc(8% - 18.4px);
  }
}

.SeSzug._7w42u8 {
  background: #1b2530;
  color: #f2f4f6;
  display: none;
}

._2wzXUU._1BYSoM {
  display: none;
}
`;

document.head.appendChild(myStyle)

let observer = new MutationObserver((mutationsList) => {
  let controls = document.getElementsByClassName('Cv6E6z');
  for (let c = 0; c < controls.length; c++) {
    if (!controls[c].hasAttribute('scrolled')) {
      controls[c].setAttribute('scrolled', 'yes')
      controls[c].click()
      setTimeout(function() {
        controls[c].parentElement.children[0].click()
      }, 1000)
    }
  }
});
observer.observe(
  document.body,
  { subtree: true, childList: true }
);
